fs   = require 'fs'
os   = require 'os'
ps   = require 'ps-node'
cp   = require 'child_process'
path = require "path"

class Process
	constructor: (args) ->
		@pid      = args.pid      or 'process.pid'
		@cwd      = args.cwd      or path.join __dirname, '../', '../'
		@cmd      = args.cmd      or 'coffee'
		@args     = args.args     or 'index.coffee'
		@stdio    = args.stdio    or 'ignore'
		@detached = args.detached or true
		@running  = false

	start: ->
		@cmd = 'coffee.cmd' if process.platform == 'win32'
		@instance = cp.spawn @cmd, [path.join @cwd, @args],
			cwd      : @cwd
			detached : @detached
			stdio    : @stdio

		@instance.unref()
		@running = true

	getPid: (path = "#{@cwd}\\#{@pid}") ->
		if fs.existsSync path
			fs.readFileSync path, 'utf8'

	stop: ()->
		if os.platform() == 'win32'
			return cp.exec "taskkill /pid #{@getPid()} /T /F"
		else
			return @instance.kill()

	status: (cb) ->
		ps.lookup pid: @getPid(), (err, res) =>
			if !err && res[0]
				@running = true
				cb res[0]
			else
				@running = false
				cb false

module.exports = Process
