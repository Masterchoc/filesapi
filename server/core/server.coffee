express   = require 'express'
cluster   = require 'cluster'
http      = require 'http'
path      = require 'path'
os        = require 'os'

middlewares = require './middlewares'
routes = require '../../routes'

class Server
	constructor: (config = {}) ->
		@port       = config.port       or 3000
		@cores      = config.cores      or 4
		@clusterize = config.clusterize or true

		if @clusterize
			if cluster.isMaster
				cluster.fork() for i in [1 .. @cores]
				@listenEvents()
			else
				@getInstance()
		else
			@getInstance()

	getApp: (cb) ->
		app    = express()
		server = http.createServer app
		app.use '/api', routes
		middlewares app


		cb app, server

	getInstance: ->
		@getApp (app, server) =>
			app.listen @port, (err) =>
				console.log 'Server listening on: ' + @port if !err

	listenEvents: ->
		cluster.on 'fork', (worker) =>
			console.log "Worker forked: #{worker.process.pid}"

		cluster.on 'listening', (worker, gateway) =>
			console.log "Worker #{worker.process.pid} listening on
			#{gateway.address}:#{gateway.port}"

		cluster.on 'exit', (worker, code, signal) =>
			console.log "Worker #{worker.process.pid} died"

module.exports = Server
