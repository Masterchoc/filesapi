helmet       = require 'helmet'
compression  = require 'compression'
bodyParser   = require 'body-parser'
cookieParser = require 'cookie-parser'

module.exports = (app) ->
	app.use helmet()
	app.use helmet.hidePoweredBy()
	app.use helmet.hsts maxAge: 5184000

	app.use compression()
	app.use bodyParser.json()
	app.use bodyParser.urlencoded extended: true
	app.use cookieParser()

