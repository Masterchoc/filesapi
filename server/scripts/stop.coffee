exec = require('child_process').exec
fs   = require 'fs'
path = '../process.pid'

if fs.existsSync path
	pid = fs.readFileSync path

	exec 'kill ' + pid, ->
		console.log "Process [#{pid}] killed."
