os = require 'os'
Process = require '../server/system/Process'

if os.platform() == 'win32'
	cwd = 'C:/wamp/www/SocialNetwork'
else
	cwd = '/srv/zebooster/server'

instance = new Process
	cwd : cwd

switch process.argv[2]
	when 'start'  then instance.start()
	when 'stop'   then instance.stop()
	when 'status' then instance.status()
