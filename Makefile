USER   = "root"
HOST   = "46.101.245.208"
NAME   = "/srv/zebooster/repository"
BRANCH = "master"
REPO   = "$(USER)@$(HOST):$(NAME)"

deploy:
	@echo "Pushing branch \"$(BRANCH)\" to production"
	git push $(REPO) $(BRANCH);
	@echo "Deploy completed !"
