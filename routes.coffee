cors     = require 'cors'
express  = require 'express'
router   = express.Router()
multer   = require 'multer'
os       = require 'os'
fs       = require 'fs'
path     = require 'path'
crypto   = require 'crypto'
config   = require './config.json'
mimetype = require 'mime-types'
disk     = require 'diskusage'

root       = if os.platform() == 'win32' then 'c:' else '/'
whitelist  = ['http://localhost:8080', 'http://zebooster.fr']
corsOptions =
	origin: (origin, cb) ->
		if whitelist.indexOf origin >= 0
			cb null, true
		else
			cb new Error 'Not allowed by CORS'

storage = multer.diskStorage
	destination: config.storage_dir,
	filename: (req, file, cb) ->
		crypto.pseudoRandomBytes 16, (err, raw) ->
			cb err if err
			cb null, raw.toString('hex') + path.extname file.originalname

upload = multer storage:storage

router.get '/download/:id/:ext', cors(corsOptions), (req, res) ->
	if typeof req.params.id != 'undefined' and typeof req.params.ext != 'undefined'
		dir  = path.join process.cwd(), config.storage_dir
		file = path.join dir, "#{req.params.id}.#{req.params.ext}"
		fs.exists file, (exists) ->
			if exists
				type = mimetype.lookup path.basename file
				if type == 'video/x-matroska'
					type = 'video/mp4'

				if type.match /image/ig
					res.download file
				else
					stat = fs.statSync file
					total = stat.size
					if req.headers.range
						range         = req.headers.range;
						parts        = range.replace(/bytes=/, "").split("-");
						partialStart = parts[0];
						partialEnd   = parts[1];

						start      = parseInt(partialStart, 10);
						end        = if partialEnd then parseInt(partialEnd, 10) else total-1;
						chunkSize  = (end - start) + 1
						readStream = fs.createReadStream file, {start: start, end: end}

						res.writeHead 206,
							'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
							'Accept-Ranges': 'bytes', 'Content-Length': chunkSize,
							'Content-Type': type
						readStream.pipe res

					else
						res.writeHead 200, { 'Content-Length': total, 'Content-Type': type }
						fs.createReadStream(file).pipe res
			else
				res.status 404
				res.end 'This file doesn\'t exists'

router.post '/upload', [cors(corsOptions), upload.any()], (req, res) ->
	if typeof req.files != 'undefined'
		if req.files.length > 0
			res.json id: req.files[0].filename.split('.')[0]
		else
			res.json error: 'No files found.'
	else
		res.json error: 'No files found.'

bytesToSize = (bytes) ->
	sizes = [
		'Octet'
		'Ko'
		'Mo'
		'Go'
		'To'
	]
	if bytes == 0
		return 'n/a'
	i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
	if i == 0
		return bytes + ' ' + sizes[i]
	(bytes / 1024 ** i).toFixed(1) + ' ' + sizes[i]


router.get '/disk_usage', (req, res) ->
	disk.check root, (err, info) ->
		console.log err if err
		if !err
			res.json
				available: bytesToSize info.available
				free: bytesToSize info.free
				total: bytesToSize info.total

module.exports = router
